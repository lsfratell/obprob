module.exports = {
  "transpileDependencies": [
    "vuetify"
  ],
  pluginOptions: {
    electronBuilder: {
      builderOptions: {
        appId: "com.frost.obprob",
        productName: "OBProb",
        copyright: "Copyright © 2020 ${author}",
        win: {
          icon: "src/assets/icon.ico"
        }
      }
    }
  }
}