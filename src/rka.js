function rkaSearch(patterns, txt, type) {
  const d = 256,
        q = 1,
        results = {};

  for (const pattern of patterns) {
    var pSize = pattern.length,
        tSize = txt.length;

    var i,
        j,
        p = 0,
        t = 0,
        h = 1;

    for (i = 0; i < pSize - 1; i++)
      h = (h * d) % q;

    for (i = 0; i < pSize; i++) {
      p = (d * p + pattern.charCodeAt(i)) % q;
      t = (d * t + pattern.charCodeAt(i)) % q;
    }

    for (i = 0; i <= tSize - pSize; i++) {
      if(p === t) {
        for (j = 0; j < pSize; j++) {
          if(txt[i + j] !== pattern[j])
            break;
        }
        if(j === pSize)
          results[i] = type;
      }
      if(i < tSize - pSize) {
        t = (d * (t - txt.charCodeAt(i) * h) + txt.charCodeAt(i + pSize)) % q;
        if(t < 0)
          t = (t + q);
      }
    }
  }

  return results;
}

function cinzaVermelhoSearch(txt) {
  if (txt.length < 6)
    return '';

  let results = '';

  for (let i = 0; i < txt.length; i += 5) {
    let window = txt.slice(i, i + 6);

    if (window.length < 6)
      break;

    if (window[4] === window[5])
      results += 'c';
    else
      results += 'v';
  }

  return results;
}

export { rkaSearch, cinzaVermelhoSearch }