import Vue from 'vue'
import Vuex from 'vuex'
import VuexPersistence from 'vuex-persist'
import { rkaSearch, cinzaVermelhoSearch } from '../rka'

Vue.use(Vuex)

var PATTERNS = {
  blue: {
    type: 'b', patterns: ['rggg', 'grrr']
  },
  pink: {
    type: 'p', patterns: ['rggr', 'grrg']
  },
  green: {
    type: 'g', patterns: ['rrgrg', 'ggrgr']
  },
  orange: {
    type: 'o', patterns: ['rrgrr', 'ggrgg']
  },
  triplication: {
    type: 't', patterns: ['pbbb', 'bppp', 'ppbpb', 'bbpbp']
  },
  ntriplication: {
    type: 'n', patterns: ['pbbp', 'bppb', 'ppbpp', 'bbpbb']
  }
}

export default new Vuex.Store({
  state: {
    candles: 'rrgggrggrrggrgrrggggrrrgrgrgrggrgrrggrgrgrrggrggrrggggggrrrggrrrrgrgrggrrgrggrgrrrgggrgrrrgrrgrrggggggggrrggrgrrgrgggrgg',
    azulrosa: '',
    verdelaranja: '',
    triplicacao: '',
    cinzavermelho: '',
    payout: 82,
    banca: 100,
    retorno: 10,
    entradas: []
  },
  plugins: [
    new VuexPersistence().plugin
  ],
  mutations: {
    updatePayout(state, value) {
      state.payout = parseFloat(value);
    },
    updateBanca(state, value) {
      state.banca = parseFloat(value);
    },
    updateRetorno(state, value) {
      state.retorno = parseFloat(value);
    },
    updateWin(state, value) {
      state.banca = parseFloat((state.banca + (value * (state.payout / 100))).toFixed(2));
    },
    updateLoss(state, value) {
      state.banca = parseFloat((state.banca - value).toFixed(2));
    },
    calcularMartingale(state) {
      state.entradas.splice(0, state.entradas.length);
      let banca = parseFloat(state.banca);
      let payout = parseFloat(state.payout) / 100;
      let retorno = parseFloat(state.retorno) / 100;

      for (let i = 0; i < 5; i++) {
        let sum = state.entradas.reduce((a, b) => a + b.entrada, 0);
        let entrada = ((banca * retorno) + sum) / payout;
        let loss = (sum + entrada) / banca;
        state.entradas.push({entrada: entrada, loss: loss});
      }
    },
    addCandle(state, value) {
      state.candles += value;
    },
    removeLastCandle(state) {
      state.candles = state.candles.slice(0, -1);
    },
    clearCandles(state) {
      state.candles = ''
    },
    searchForGreenOrange(state) {
      state.verdelaranja = '';

      let green = rkaSearch(PATTERNS.green.patterns, state.candles, PATTERNS.green.type),
          orange = rkaSearch(PATTERNS.orange.patterns, state.candles, PATTERNS.orange.type);

      let patterns = Object.assign({}, green, orange);

      for (const key in patterns) {
        if (patterns.hasOwnProperty.call(patterns, key))
          state.verdelaranja += patterns[key]
      }
    },
    searchForBluePink(state) {
      state.azulrosa = '';

      let blue = rkaSearch(PATTERNS.blue.patterns, state.candles, PATTERNS.blue.type),
          pink = rkaSearch(PATTERNS.pink.patterns, state.candles, PATTERNS.pink.type);

      let patterns = Object.assign({}, blue, pink);

      for (const key in patterns) {
        if (patterns.hasOwnProperty.call(patterns, key))
          state.azulrosa += patterns[key]
      }
    },
    searchForTriplication(state) {
      state.triplicacao = '';

      let triplication = rkaSearch(PATTERNS.triplication.patterns, state.azulrosa, PATTERNS.triplication.type),
          ntriplication = rkaSearch(PATTERNS.ntriplication.patterns, state.azulrosa, PATTERNS.ntriplication.type);

      let patterns = Object.assign({}, triplication, ntriplication)

      for (const key in patterns) {
        if (patterns.hasOwnProperty.call(patterns, key))
          state.triplicacao += patterns[key]
      }
    },
    searchForCinzaVermelho(state) {
      state.cinzavermelho = cinzaVermelhoSearch(state.candles);
    }
  }
})
